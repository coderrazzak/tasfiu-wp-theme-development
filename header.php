<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tasfiu - Corporate HTML Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.ico">
    <link rel="apple-touch-icon" href="images/logo/apple-touch-icon.png">

    <?php wp_head();?>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Body main wrapper start -->
<div class="wrapper">
    <!-- Start of Header Top Area -->
    <header id="header-content">
        <!-- Start Header Top -->
        <div class="header-top-area bg-theme">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <ul class="header-left">
                            <li>Call Us Today! 2.123.123.123</li>
                            <li><a href="mailto:info@gmail.com" target="_top">info@gmail.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <div class="header-right">
                            <ul class="social-icon">
                                <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-tumblr"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-dribbble"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top -->
        <!-- Start of mainmenu area -->
        <div id="sticky-header-with-topbar" class="header-bottom-area bg-white height-100  transparent-header hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="logo f-left sm-center">
                            <a href="index.html">
                                <img src="<?php echo get_template_directory_uri();?>/images/logo/tasfiu.png" alt="logo image">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-12">
                        <?php
                            wp_nav_menu(array(
                                'theme_location'    => 'main-menu',
                                'container'         => 'div',
                                'container_class'   => 'mainmenu-container',
                                'menu_class'        => 'main-menu',
                                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                'walker'            => new WP_Bootstrap_Navwalker(),
                                'depth'             => 3
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile-menu-area start -->
        <div class="mobile-menu-area hidden-md hidden-lg hidden-sm">
            <div class="fluid-container mobile-menu-container">
                <div class="mobile-logo"><a href="index.html"><img src="<?php echo get_template_directory_uri();?>/images/logo/2.png" alt="Mobile logo"></a></div>
                <div class="mobile-menu clearfix">
                    <nav id="mobile_dropdown">
                        <ul>
                            <li><a href="index.html">Home</a> </li>
                            <li><a href="blog-details.html">blog</a>
                                <ul>
                                    <li><a href="blog-page.html">blog pages</a></li>
                                    <li><a href="blog-details.html">blog details</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pages</a>
                                <ul>
                                    <li><a href="about.html">ABOUT US</a></li>
                                    <li><a href="portfolio.html">PORTFOLIO</a></li>
                                    <li><a href="brand.html">brand</a></li>
                                    <li><a href="our-service.html">our service</a></li>
                                    <li><a href="our-team.html">our team</a></li>
                                    <li><a href="our-work.html">our work</a></li>
                                    <li><a href="progress-bars.html">progress bars</a></li>
                                    <li><a href="testimonial.html">testimonial</a></li>
                                    <li><a href="single-portfolio.html">Single Portfolio</a></li>
                                </ul>
                            </li>
                            <li><a href="portfolio.html">Portfolio</a>
                            </li>
                            <li><a href="contact.html">Contact</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Mobile-menu-area end -->
    </header>
    <!-- End of Header Top Area -->