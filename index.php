<?php get_header();?>

<!-- Start of Blog Area -->
<section id="tf-blog-area" class="our-blog-area bg-white ptb-100 xtb-30">
    <div class="container">
        <!-- start section title -->
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="section-title text-center">
                    <h2 class="title-line">OUR BLOG</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing</p>
                </div>
            </div>
        </div>
        <!-- end section title -->
        <div class="row">
            <div class="single-blog-wrap single-blog-wrap-2 mt-40 clearfix">

                <?php
                    if  (have_posts()) :
                    while(have_posts()) : the_post();
                ?>

                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="tf-single-blog">
                        <div class="tf-blog">
                            <div class="tf-blog-front">
                                <img class="img-responsive" src="<?php echo get_the_post_thumbnail();?>" alt="blog img">
                            </div>
                            <div class="tf-blog-hover-information">
                                <div class="bolg-action">
                                    <a href="<?php echo get_the_permalink();?>"><i class="zmdi zmdi-link"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="tf-blog-body">
                            <span class="tf-blog-date"><?php echo get_the_date('F j Y');?></span>
                            <h2>
                                <a href="<?php echo get_the_permalink();?>">
                                    <?php echo get_the_title();?>
                                </a>
                            </h2>
                            <p><?php echo wp_trim_words(get_the_content(), 10, '..')?></p>
                        </div>
                    </div>
                </div>

                <?php endwhile; ?>
                <!-- End blog area -->
                <!-- End blog area -->
            </div>
        </div>
    </div>
    <?php
    else:
        get_template_part('template-parts/pages/content', 'none');
        wp_reset_postdata();
    endif;
    ?>
    <!-- start pagination -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    the_posts_pagination(array(
                            'mid-size'      => 2,
                            'prev_text'     => __('Back', 'tasfiu'),
                            'next_text'     => __('<i class="zmdi zmdi-chevron-right"></i>', 'tasfiu')
                    ));
                ?>
            </div>
        </div>
    </div>
    <!-- End pagination -->
</section>
<!-- End of Blog Area -->


<?php get_footer(); ?>