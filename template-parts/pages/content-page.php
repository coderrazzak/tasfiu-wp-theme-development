<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Tasfiu
 * @since 1.0
 * @version 1.0
 */
?>

<div class="page-sec">
    <?php
        the_content();
        wp_link_pages(array(
            'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
            'after'  => '</div>',
        ));
    ?>
</div>
