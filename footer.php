
<!-- Start of Footer Area -->
<footer id="footer" class="footer-area footer-bg pt-100">
    <div class="footer-top-area pb-50">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-12">
                    <div class="foo-single-widget">
                        <div class="footer-title">
                            <h2>Customer Care</h2>
                        </div>
                        <ul>
                            <li><a href="#">Terms & Condition</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Shipping Charge</a></li>
                            <li><a href="#">Shipping Tract</a></li>
                            <li><a href="#">Payment Method</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1 col-sm-6 col-xs-12">
                    <div class="foo-single-widget border-left">
                        <div class="footer-title">
                            <h2>Your Account</h2>
                        </div>
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Affiliate Dashboard</a></li>
                            <li><a href="#">Billing Address</a></li>
                            <li><a href="#">Cancel Order</a></li>
                            <li><a href="#">Cancel Order</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1 col-sm-6 col-xs-12">
                    <div class="foo-single-widget border-left smmt-30">
                        <div class="footer-title">
                            <h2>Shop Information</h2>
                        </div>
                        <ul>
                            <li><a href="#">About company</a></li>
                            <li><a href="#">Become Member</a></li>
                            <li><a href="#">License Details</a></li>
                            <li><a href="#">Tax Information</a></li>
                            <li><a href="#">Job & Vacancics</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1 col-lg-3 col-lg-offset-1 col-sm-6 col-xs-12">
                    <div class="foo-single-widget border-left smmt-30">
                        <div class="logo">
                            <img src="<?php echo get_template_directory_uri();?>/images/logo/2.png" alt="logo images">
                        </div>
                        <p class="ft-pra-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna the aliqua. Ut enim ad minim commodo consequat.</p>
                        <p class="ft-pra-2">404, Mark Twain Tower, Saint MG Road,
                            United States</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="copy-right-text">
                        <p>Copyright © 2018 <a href="https://freethemescloud.com/" target="_blank">Free themes Cloud</a></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <ul class="social-icon">
                        <li><a href="#"><i class="zmdi zmdi-facebook"></i>
                            </a></li>
                        <li><a href="#"><i class="zmdi zmdi-tumblr"></i>
                            </a></li>
                        <li><a href="#"><i class="zmdi zmdi-linkedin"></i>
                            </a></li>
                        <li><a href="#"><i class="zmdi zmdi-rss"></i></a>
                        </li>
                        <li><a href="#"><i class="zmdi zmdi-dribbble"></i>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--End of Footer Area -->
<!--start back to top -->
<div class="hidden-xs" id="back-top"><i class="zmdi zmdi-chevron-up"></i></div>
<!-- End back to top -->
</div>

<?php wp_footer();?>
</body>
</html>