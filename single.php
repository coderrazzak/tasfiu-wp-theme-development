<?php get_header();?>



<!-- Start of Blog Details Area -->
<div class="our-blog-details-area bg-white">
    <div class="container">
        <div class="row">
            <!--Start Blog Left Sidebar -->
            <?php
                if (have_posts()) :
                    while (have_posts()) : the_post();
            ?>
            <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                <div class="blog-details-left-sidebar">
                    <!-- Start Blog-Details Top -->
                    <div class="bolg-details-inner pt-100 smpt-60 xpt-60">
                        <div class="tf-blog-thumb">
                            <img src="<?php echo get_the_post_thumbnail();?>" alt=" blog image">
                        </div>
                        <div class="gray-blog-details bg-gray">
                            <div class="tf-blog-title">
                                <h2><?php echo get_the_title();?></h2>
                            </div>
                            <div class="tf-blogdeatils-meta">
                                <span class="tf-blog-date"><?php echo get_the_date('F j Y');?></span>
                                <div class="tf-blog-meta">
                                    <ul>
                                        <li><a href="#"><i class="zmdi zmdi-eye"></i>35</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-comments"></i>20</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tf-blog-info">
                                <p><?php the_content();?></p>
                            </div>
                            <div class="postandshare">
                                <div class="post"><span class="meta-title">Posted In.unecloding</span>
                                </div>
                                <div class="tf-blog-share">
                                    <div class="post"><span class="meta-title">Share:</span>
                                        <ul class="social-icon">
                                            <li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>"><i class="zmdi zmdi-facebook"></i>
                                                </a></li>
                                            <li><a href="https://twitter.com/home?status=<?php echo get_permalink( get_the_ID() ); ?>"><i class="zmdi zmdi-tumblr"></i>
                                                </a></li>
                                            <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=&lt;?php the_permalink() ?&gt;&amp;title=&lt;?php the_title(); ?&gt;&amp;summary=&amp;source=&lt;?php bloginfo('name'); ?&gt;"><i class="zmdi zmdi-linkedin"></i>
                                                </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Blog-Left Top -->
                </div>
            </div>
            <?php endwhile; endif; wp_reset_postdata();?>
            <!--End Blog Left Sidebar -->
            <!--Start Blog Right Sidebar -->
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                <div class="blog-details-right-sidebar pt-100 smpt-60 smpb-60 xpt-0">
                    <!-- Input area -->
                    <div class="input-area">
                        <form>
                            <input type="text" placeholder="Search......"> <button class="tf-input-btn" type="submit"><i class="zmdi zmdi-search"></i></button>
                        </form>
                    </div>
                    <!--Start Categore area -->
                    <div class="tf-category-area">
                        <h2 class="tf-cat-title">CATEGORY</h2>
                        <ul class="category-menu">
                            <li><a href="#">Photoshop<span>25</span></a></li>
                            <li><a href="#">Trending<span>25</span></a></li>
                            <li><a href="#">WordPress<span>25</span></a></li>
                            <li><a href="#">HTML<span>25</span></a></li>
                            <li><a href="#">Illustrator<span>25</span></a></li>
                        </ul>
                    </div>
                    <!--End Categore area -->
                    <!--Start recent Post Area -->
                    <div class="recent-post-container mt-40 mb-40">
                        <h2 class="post-title">Recent Post</h2>
                        <!--Start Single-Post -->
                        <div class="recent-post-area">
                            <div class="single-post">
                                <div class="post-img">
                                    <img src="images/post-img/1.jpg" alt="post img">
                                </div>
                                <div class="single-post-details">
                                    <h2><a href="blog-right-sidebar.html">It Is A Fantstic City Visit</a></h2>
                                    <ul class="sing-blog-meta">
                                        <li><a href="#"><i class="zmdi zmdi-eye"></i>35</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-comments"></i>20</a></li>
                                    </ul>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor</p>
                                </div>
                            </div>
                        </div>
                        <!--End Single-Post -->
                        <!--Start Single-Post -->
                        <div class="recent-post-area">
                            <div class="single-post">
                                <div class="post-img">
                                    <img src="images/post-img/2.jpg" alt="post img">
                                </div>
                                <div class="single-post-details">
                                    <h2><a href="blog-right-sidebar.html">It Is A Fantstic City Visit</a></h2>
                                    <ul class="sing-blog-meta">
                                        <li><a href="#"><i class="zmdi zmdi-eye"></i>35</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-comments"></i>20</a></li>
                                    </ul>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor</p>
                                </div>
                            </div>
                        </div>
                        <!--End Single-Post -->
                        <!--Start Single-Post -->
                        <div class="recent-post-area">
                            <div class="single-post">
                                <div class="post-img">
                                    <img src="images/post-img/3.jpg" alt="post img">
                                </div>
                                <div class="single-post-details">
                                    <h2><a href="blog-right-sidebar.html">It Is A Fantstic City Visit</a></h2>
                                    <ul class="sing-blog-meta">
                                        <li><a href="#"><i class="zmdi zmdi-eye"></i>35</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-comments"></i>20</a></li>
                                    </ul>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor</p>
                                </div>
                            </div>
                        </div>
                        <!--End Single-Post -->
                        <!--Start Single-Post -->
                        <div class="recent-post-area">
                            <div class="single-post">
                                <div class="post-img">
                                    <img src="images/post-img/1.jpg" alt="post img">
                                </div>
                                <div class="single-post-details">
                                    <h2><a href="blog-right-sidebar.html">It Is A Fantstic City Visit</a></h2>
                                    <ul class="sing-blog-meta">
                                        <li><a href="#"><i class="zmdi zmdi-eye"></i>35</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-comments"></i>20</a></li>
                                    </ul>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor</p>
                                </div>
                            </div>
                        </div>
                        <!--End Single-Post -->
                    </div>
                    <!--End recent Post Area -->
                    <!--Start Tags Post Area -->
                    <div class="tag-area">
                        <h2 class="post-title">Search by Tags</h2>
                        <ul class="share-tags">
                            <?php
                                the_tags('<li>', '</li><li>', '</li>');
                            ?>
                        </ul>
                    </div>
                    <!--End Tags Post Area -->
                </div>
            </div>
            <!--End Blog Right Sidebar -->
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 ">
                <div>
                    <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'),get_the_author_meta('user_nicenaem'))?>"> Post By: <h5 style="display: inline-block;"><?php the_author();?></h5></a>
                </div>
                <div class="author-meta">
                    <?php echo get_avatar(get_the_author_meta('ID'), 100);?>
                    <p><?php echo get_the_author_meta('description')?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of Blog Details Area -->




<?php get_footer();?>

