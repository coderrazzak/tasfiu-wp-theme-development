<?php get_header(); ?>
        <!-- Start of Slider Area -->
        <div id="tf-slider" class="tf-slider-area">
            <div class="tf-sliders slider-owl-active">
                <!-- Start Single Slider Area -->
                <div class="slider-area slider-bg-1" data-black-overlay="5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="slider-content">
                                    <div class="slider-content-innner text-white">
                                        <h3>WORK SHEDULE</h3>
                                        <h1>FOR COMPANY DEAL</h1>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ma aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p>
                                        <div class="tsf-button">
                                            <a href="#" class="tf-button">BY THEME</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End Single Slider Area --> 
                <!-- Start Single Slider Area -->
                <div class="slider-area slider-bg-2" data-black-overlay="5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="slider-content">
                                    <div class="slider-content-innner text-white">
                                        <h3>WORK SHEDULE TWO</h3>
                                        <h1>FOR COMPANY DEAL</h1>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ma aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p>
                                        <div class="tsf-button">
                                            <a href="#" class="tf-button">BY THEME</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End Single Slider Area --> 
                <!-- Start Single Slider Area -->
                <div class="slider-area slider-bg-4" data-black-overlay="5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="slider-content">
                                    <div class="slider-content-innner text-white">
                                        <h3>WORK SHEDULE TWO</h3>
                                        <h1>FOR COMPANY DEAL</h1>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore ma aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p>
                                        <div class="tsf-button">
                                            <a href="#" class="tf-button">BY THEME</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End Single Slider Area -->                 
            </div>
        </div>
        <!-- End of Slider Area -->
        <!-- Start of Service Area -->
        <section class="our-service-area ptb-100 bg-white xtb-60">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="section-title text-center">
                            <h2 class="title-line">FEATURES LIST</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--Start Single service -->   
                    <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12">
                        <div class="single-servic">
                            <div class="service-inner">
                                <i class="flaticon-light-bulb icon"></i>
                                <h4 class="service-title">Bright Color Features</h4>
                                <p>Lorem Ipsum is Ipsum is simply of the printing and typesetting industryIpsum is simply</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single service -->
                    <!--Start Single service -->   
                    <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12">
                        <div class="single-servic">
                            <div class="service-inner">
                                <i class="flaticon-edit icon"></i>
                                <h4 class="service-title">Amazing Text Editor</h4>
                                <p>Lorem Ipsum is Ipsum is simply of the printing and typesetting industryIpsum is simply</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single service -->
                    <!--Start Single service -->   
                    <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12">
                        <div class="single-servic">
                            <div class="service-inner">
                                <i class="flaticon-umbrella icon"></i>
                                <h4 class="service-title">Alaways Support</h4>
                                <p>Lorem Ipsum is Ipsum is simply of the printing and typesetting industryIpsum is simply</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single service -->
                    <!--Start Single service -->   
                    <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12">
                        <div class="single-servic">
                            <div class="service-inner">
                                <i class="flaticon-alarm-clock icon"></i>
                                <h4 class="service-title">Time Shedule Work</h4>
                                <p>Lorem Ipsum is Ipsum is simply of the printing and typesetting industryIpsum is simply</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single service -->
                    <!--Start Single service -->   
                    <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12">
                        <div class="single-servic">
                            <div class="service-inner">
                                <i class="flaticon-internet icon"></i>
                                <h4 class="service-title">Target Fillup Work</h4>
                                <p>Lorem Ipsum is Ipsum is simply of the printing and typesetting industryIpsum is simply</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single service -->
                    <!--Start Single service -->   
                    <div class="col-md-3 col-sm-6 col-lg-4 col-xs-12">
                        <div class="single-servic">
                            <div class="service-inner">
                                <i class="flaticon-prize icon"></i>
                                <h4 class="service-title">Win The Award</h4>
                                <p>Lorem Ipsum is Ipsum is simply of the printing and typesetting industryIpsum is simply</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single service -->
                </div>
             </div>
        </section>
        <!-- End of Service Area -->
        <!-- Start of About Area -->
        <section id="tf-about-container" class="tf-about-area ptb-100 bg-gray xtb-60">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title text-left">
                                    <h2 class="title-line">FILL OUR WORK</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing</p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="about-detail">
                                    <p class="ad-first-pra">The best of the best is combined in Tasfiu. It features fine aesthetics, strong functional backend, plenty of customizable elements, a vast array of theme customization options and last but not least a 5 star support </p>

                                    <p class="ad-second-pra">Not only is Tasfiu fully responsive and looks great on different devices but you can customize each element’s behavior on distinct </p>
                                    <div class="tsf-button">
                                        <a href="#" class="tf-button">BY THEME</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <div class="about-img">
                            <img src="<?php echo get_template_directory_uri();?>/images/about/1.jpg" alt="about images">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End of About Area -->
        <!-- Start of Skill Area -->
        <section class="our-skill-area bg-white ptb-100 xtb-60">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <div class="section-title text-center">
                                    <h2 class="title-line">WE ARE EXPERTS</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <div class="fs-skill-container pt-10">
                                    <!-- Start single skill -->
                                    <div class="single-skill">
                                        <p>PHOTOSHOP</p>
                                        <div class="progress">
                                            <div class="progress-bar wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay=".5s" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%"><span class="pen-lable pro-bg-1">60%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End single skill -->
                                    <!-- Start single skill -->
                                    <div class="single-skill">
                                        <p>HTML5</p>
                                        <div class="progress">
                                            <div class="progress-bar wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay=".5s" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"><span class="pen-lable pro-bg-2">70%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End single skill -->
                                    <!-- Start single skill -->
                                    <div class="single-skill">
                                        <p>ILLUSTRATOR</p>
                                        <div class="progress">
                                            <div class="progress-bar wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay=".5s" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%"><span class="pen-lable pro-bg-3">90%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End single skill -->
                                    <!-- Start single skill -->
                                    <div class="single-skill">
                                        <p>JOOMLA</p>
                                        <div class="progress">
                                            <div class="progress-bar wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay=".5s" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:75%"><span class="pen-lable pro-bg-4">75%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End single skill -->
                                    <!-- Start single skill -->
                                    <div class="single-skill">
                                        <p>WORDPRESS</p>
                                        <div class="progress">
                                            <div class="progress-bar wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay=".5s" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%"><span class="pen-lable pro-bg-5">80%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End single skill -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- End of Skill Area -->
        <!-- Start of Amazing Work Area -->
        <section id="amazing-work-area" class="bg-gray xtb-0">
            <div class="amazing-work-inner">
                <div class="width-half">
                    <div class="amazing-work-images">
                        <img src="<?php echo get_template_directory_uri();?>/images/others/1.jpg" alt="amazing images">
                    </div>
                </div>
                <div class="width-half">
                    <div class="amz-work-contents">
                        <div class="section-title text-center">
                            <h2 class="title-line">TASFIU IS AMAZING</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing</p>
                        </div>
                        <div class="amz-work-container clearfix">
                            <!-- start single-work-details -->
                            <div class="single-work-details">
                                <div class="amz-icon">
                                    <i class="flaticon-internet icon"></i>
                                </div>
                                <div class="amz-details">
                                    <h4>Huge Tools</h4>
                                    <p>Lorem Ipsum is simply dummy text  the printing and typesetting industry. It has survived not duic</p>
                                </div>        
                            </div>
                            <!-- End single-work-details -->
                            <!-- start single-work-details -->
                            <div class="single-work-details">
                                <div class="amz-icon">
                                    <i class="flaticon-watches icon"></i>
                                </div>
                                <div class="amz-details">
                                    <h4>Timely Work</h4>
                                    <p>Lorem Ipsum is simply dummy text  the printing and typesetting industry. It has survived not duic</p>
                                </div>        
                            </div>
                            <!-- End single-work-details -->
                            <!-- start single-work-details -->
                            <div class="single-work-details">
                                <div class="amz-icon">
                                    <i class="flaticon-worldwide icon"></i>
                                </div>
                                <div class="amz-details">
                                    <h4>Global Marketing</h4>
                                    <p>Lorem Ipsum is simply dummy text  the printing and typesetting industry. It has survived not duic</p>
                                </div>        
                            </div>
                            <!-- End single-work-details -->
                            <!-- start single-work-details -->
                            <div class="single-work-details">
                                <div class="amz-icon">
                                    <i class="flaticon-headphones-1 icon"></i>
                                </div>
                                <div class="amz-details">
                                    <h4>24 Hours Support</h4>
                                    <p>Lorem Ipsum is simply dummy text  the printing and typesetting industry. It has survived not duic</p>
                                </div>        
                            </div>
                            <!-- End single-work-details -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End of Amazing Work Area -->
        <!-- Start of Portfolio Area -->
        <section id="tf-portfolio-area" class="tf-portfolio-wrap bg-white ptb-100 xtb-60">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="section-title text-center">
                            <h2 class="title-line">OUR RELIABLE WORK</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing</p>
                        </div>
                    </div>
                </div>
                <div class="row mb-50 xmb-20">
                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 text-center">
                        <ul id="tf-filters" class="tf-port-filter-nav">
                            <li data-filter="*" class="is-checked">All</li>
                            <li data-filter=".webdesign">Web</li>
                            <li data-filter=".photography">Photography</li>
                            <li data-filter=".html">html</li>
                            <li data-filter=".jomla">jomla</li>
                            <li data-filter=".wordpress">Wordpress</li>
                        </ul>
                    </div>
                </div>
                <div class="tf-portfolio-row">
                    <div class="tf-portfolio-container clearfix">
                        <!-- Start single portfolio -->
                        <div class="pro-item nopadding col-md-4 col-sm-6 col-lg-4 col-xs-12 html jomla wordpress">
                            <div class="tf-portfolio" data-title-position="left, top">
                                <div class="tf-port-thumb">
                                    <img src="<?php echo get_template_directory_uri();?>/images/portfolio/1.jpg" alt="port img">
                                </div>
                                <div class="tf-portfolio-hover-information">
                                    <div class="tf-port-info">
                                        <h2 class="tf-port-title"><a href="single-portfolio.html">Startups and Tech Companics</a></h2>
                                        <a class="tf-port-category" href="#">Branding</a>
                                    </div>
                                    <div class="tf-port-action">
                                        <a href="images/portfolio/2.jpg" data-lightbox="tfportimg" data-title="My caption"><i class="zmdi zmdi-zoom-in"></i></a>
                                        <a href="single-portfolio.html"><i class="zmdi zmdi-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End single portfolio -->
                        <!-- Start single portfolio -->
                        <div class="pro-item nopadding col-md-4 col-sm-6 col-lg-4 col-xs-12 html jomla wordpress">
                            <div class="tf-portfolio" data-title-position="center, top">
                                <div class="tf-port-thumb">
                                    <img src="<?php echo get_template_directory_uri();?>/images/portfolio/2.jpg" alt="port img">
                                </div>
                                <div class="tf-portfolio-hover-information">
                                    <div class="tf-port-info">
                                        <h2 class="tf-port-title"><a href="single-portfolio.html">Startups and Tech Companics</a></h2>
                                        <a class="tf-port-category" href="#">Branding</a>
                                    </div>
                                    <div class="tf-port-action">
                                        <a href="images/portfolio/2.jpg" data-lightbox="tfportimg" data-title="My caption"><i class="zmdi zmdi-zoom-in"></i></a>
                                        <a href="single-portfolio.html"><i class="zmdi zmdi-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End single portfolio -->
                        <!-- Start single portfolio -->
                        <div class="pro-item nopadding col-md-4 col-sm-6 col-lg-4 col-xs-12 webdesign jomla">
                            <div class="tf-portfolio"  data-title-position="right, top">
                                <div class="tf-port-thumb">
                                    <img src="<?php echo get_template_directory_uri();?>/images/portfolio/3.jpg" alt="port img">
                                </div>
                                <div class="tf-portfolio-hover-information">
                                    <div class="tf-port-info">
                                        <h2 class="tf-port-title"><a href="single-portfolio.html">Startups and Tech Companics</a></h2>
                                        <a class="tf-port-category" href="#">Branding</a>
                                    </div>
                                    <div class="tf-port-action">
                                        <a href="images/portfolio/3.jpg" data-lightbox="tfportimg" data-title="My caption"><i class="zmdi zmdi-zoom-in"></i></a>
                                        <a href="single-portfolio.html"><i class="zmdi zmdi-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End single portfolio -->
                        <!-- Start single portfolio -->
                        <div class="pro-item nopadding col-md-4 col-sm-6 col-lg-4 col-xs-12 jomla photography html wordpress">
                            <div class="tf-portfolio" data-title-position="left, bottom">
                                <div class="tf-port-thumb">
                                    <img src="<?php echo get_template_directory_uri();?>/images/portfolio/4.jpg" alt="port img">
                                </div>
                                <div class="tf-portfolio-hover-information">
                                    <div class="tf-port-info">
                                        <h2 class="tf-port-title"><a href="single-portfolio.html">Startups and Tech Companics</a></h2>
                                        <a class="tf-port-category" href="#">Branding</a>
                                    </div>
                                    <div class="tf-port-action">
                                        <a href="images/portfolio/4.jpg" data-lightbox="tfportimg" data-title="My caption"><i class="zmdi zmdi-zoom-in"></i></a>
                                        <a href="single-portfolio.html"><i class="zmdi zmdi-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End single portfolio -->
                        <!-- Start single portfolio -->
                        <div class="pro-item nopadding col-md-4 col-sm-6 col-lg-4 col-xs-12 webdesign wordpress">
                            <div class="tf-portfolio" data-title-position="center, bottom">
                                <div class="tf-port-thumb">
                                    <img src="<?php echo get_template_directory_uri();?>/images/portfolio/5.jpg" alt="port img">
                                </div>
                                <div class="tf-portfolio-hover-information">
                                    <div class="tf-port-info">
                                        <h2 class="tf-port-title"><a href="single-portfolio.html">Startups and Tech Companics</a></h2>
                                        <a class="tf-port-category" href="#">Branding</a>
                                    </div>
                                    <div class="tf-port-action">
                                        <a href="images/portfolio/5.jpg" data-lightbox="tfportimg" data-title="My caption"><i class="zmdi zmdi-zoom-in"></i></a>
                                        <a href="single-portfolio.html"><i class="zmdi zmdi-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End single portfolio -->
                        <!-- Start single portfolio -->
                        <div class="pro-item nopadding col-md-4 col-sm-6 col-lg-4 col-xs-12 jomla photography html wordpress">
                            <div class="tf-portfolio" data-title-position="right, bottom">
                                <div class="tf-port-thumb">
                                    <img src="<?php echo get_template_directory_uri();?>/images/portfolio/6.jpg" alt="port img">
                                </div>
                                <div class="tf-portfolio-hover-information">
                                    <div class="tf-port-info">
                                        <h2 class="tf-port-title"><a href="single-portfolio.html">Startups and Tech Companics</a></h2>
                                        <a class="tf-port-category" href="#">Branding</a>
                                    </div>
                                    <div class="tf-port-action">
                                        <a href="images/portfolio/6.jpg" data-lightbox="tfportimg" data-title="My caption"><i class="zmdi zmdi-zoom-in"></i></a>
                                        <a href="single-portfolio.html"><i class="zmdi zmdi-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End single portfolio -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End of Portfolio Area -->
        <!-- Start of Blog Area -->
        <section id="tf-blog-area" class="our-blog-area bg-gray ptb-100 xtb-30">
            <div class="container">
                <!-- start section title -->
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="section-title text-center">
                            <h2 class="title-line">OUR BLOG</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing</p>
                        </div>
                    </div>
                </div>
                <!-- end section title -->
                
                <div class="row">
                    <div class="single-blog-wrap mt-40 clearfix">
                        <!-- single blog area -->
                        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                            <div class="tf-single-blog">
                                <div class="tf-blog">
                                    <div class="tf-blog-front">
                                        <img src="<?php echo get_template_directory_uri();?>/images/blog/1.jpg" alt="blog img">
                                    </div>
                                    <div class="tf-blog-hover-information">
                                        <div class="bolg-action">
                                            <a href="blog-details.html"><i class="zmdi zmdi-link"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tf-blog-body">
                                    <span class="tf-blog-date">04 Aug 2015 Technology</span>
                                    <h2><a href="blog-details.html">It Is A Fantastic City Visit</a></h2>
                                    <p>Far far awaya the word mountains,far from the countr....</p>
                                </div>
                            </div>
                        </div>
                        <!-- End blog area -->
                        <!-- single blog area -->
                        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                            <div class="tf-single-blog">
                                <div class="tf-blog">
                                    <div class="tf-blog-front">
                                        <img src="<?php echo get_template_directory_uri();?>/images/blog/2.jpg" alt="blog img">
                                    </div>
                                    <div class="tf-blog-hover-information">
                                        <div class="bolg-action">
                                            <a href="blog-details.html"><i class="zmdi zmdi-link"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tf-blog-body">
                                    <span class="tf-blog-date">04 Aug 2015 Technology</span>
                                    <h2><a href="blog-details.html">It Is A Fantastic City Visit</a></h2>
                                    <p>Far far awaya the word mountains,far from the countr....</p>
                                </div>
                            </div>
                        </div>
                        <!-- End blog area -->
                        <!-- single blog area -->
                        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                            <div class="tf-single-blog">
                                <div class="tf-blog">
                                    <div class="tf-blog-front">
                                        <img src="<?php echo get_template_directory_uri();?>/images/blog/1.jpg" alt="blog img">
                                    </div>
                                    <div class="tf-blog-hover-information">
                                        <div class="bolg-action">
                                            <a href="blog-details.html"><i class="zmdi zmdi-link"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tf-blog-body">
                                    <span class="tf-blog-date">04 Aug 2015 Technology</span>
                                    <h2><a href="blog-details.html">It Is A Fantastic City Visit</a></h2>
                                    <p>Far far awaya the word mountains,far from the countr....</p>
                                </div>
                            </div>
                        </div>
                        <!-- End blog area -->
                        <!-- single blog area -->
                        <div class="hidden-md hidden-lg col-sm-6 hidden-xs">
                            <div class="tf-single-blog">
                                <div class="tf-blog">
                                    <div class="tf-blog-front">
                                        <img src="<?php echo get_template_directory_uri();?>/images/blog/1.jpg" alt="blog img">
                                    </div>
                                    <div class="tf-blog-hover-information">
                                        <div class="bolg-action">
                                            <a href="blog-details.html"><i class="zmdi zmdi-link"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tf-blog-body">
                                    <span class="tf-blog-date">04 Aug 2015 Technology</span>
                                    <h2><a href="blog-details.html">It Is A Fantastic City Visit</a></h2>
                                    <p>Far far awaya the word mountains,far from the countr....</p>
                                </div>
                            </div>
                        </div>
                        <!-- End blog area -->          
                    </div>
                </div>
            </div>
        </section>
        <!-- End of Blog Area -->
        <!-- Start of Brand Area -->
        <div class="tf-brand-area ptb-100 bg-white xtb-60">
            <div class="container">
                <div class="brand-row">
                    <div class="brand-wrap">
                        <div class="single-brand">
                            <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/brand/1.png" alt="brand images"></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/brand/2.png" alt="brand images"></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/brand/3.png" alt="brand images"></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/brand/4.png" alt="brand images"></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/brand/5.png" alt="brand images"></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/brand/6.png" alt="brand images"></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/brand/1.png" alt="brand images"></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/brand/7.png" alt="brand images"></a>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Brand Area -->
        <!-- Start of Contact Area -->
        <div class="our-contact-area pb-100 smpb-60 xspb-60">
            <div class="container">
                <div class="row">
                    <!-- Start Single Contact Area -->
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <div class="our-contact-inner">
                            <div class="contact-us">
                                <i class="flaticon-male-telemarketer icon"></i>
                                <div class="contact-title">
                                    <h2>QUICK HELP</h2>
                                    <h4>Emergency Cases</h4>
                                </div>
                            </div>
                            <p class="contact-pra">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#">CONTACT NOW<i class="zmdi zmdi-arrow-right"></i></a>
                        </div>
                    </div>
                    <!-- End Single Contact Area -->
                    <!-- Start Single Contact Area -->
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <div class="our-contact-inner">
                            <div class="contact-us">
                                <i class="flaticon-idea icon"></i>
                                <div class="contact-title">
                                    <h2>SMOOTH AND EASY</h2>
                                    <h4>SMOOTH AND EASY</h4>
                                </div>
                            </div>
                            <p class="contact-pra">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#">CONTACT NOW<i class="zmdi zmdi-arrow-right"></i></a>
                        </div>
                    </div>
                    <!-- End Single Contact Area -->
                    <!-- Start Single Contact Area -->
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <div class="our-contact-inner">
                            <div class="contact-us">
                                <i class="flaticon-earth-globe-symbol-of-grid icon"></i>
                                <div class="contact-title">
                                    <h2>IN YOUR COUNTRY</h2>
                                    <h4>360 Branches World wide</h4>
                                </div>
                            </div>
                            <p class="contact-pra">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#">CONTACT NOW<i class="zmdi zmdi-arrow-right"></i></a>
                        </div>
                    </div>
                    <!-- End Single Contact Area -->
                    <!-- Start Single Contact Area -->
                    <div class="hidden-md hidden-lg col-sm-6 hidden-xs">
                        <div class="our-contact-inner">
                            <div class="contact-us">
                                <i class="flaticon-earth-globe-symbol-of-grid icon"></i>
                                <div class="contact-title">
                                    <h2>IN YOUR COUNTRY</h2>
                                    <h4>360 Branches World wide</h4>
                                </div>
                            </div>
                            <p class="contact-pra">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#">CONTACT NOW<i class="zmdi zmdi-arrow-right"></i></a>
                        </div>
                    </div>
                    <!-- End Single Contact Area -->
                </div>
            </div>
        </div>
        <!-- End of Contact Area -->
<?php get_footer();?>