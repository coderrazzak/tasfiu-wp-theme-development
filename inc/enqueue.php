<?php

function tasfiu_css_js(){
    // enqueue css files
    wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', array(), '3.3.7', 'all');
    wp_enqueue_style('core', get_template_directory_uri().'/css/core.css', array(), '3.3.7', 'all');
    wp_enqueue_style('shortcodes', get_template_directory_uri().'/css/shortcode/shortcodes.css', array(), '3.3.7', 'all');
    wp_enqueue_style('style', get_template_directory_uri().'/style.css', array(), '3.3.7', 'all');
    wp_enqueue_style('responsive', get_template_directory_uri().'/css/responsive.css', array(), '3.3.7', 'all');
    wp_enqueue_style('custom', get_template_directory_uri().'/css/custom.css', array(), '3.3.7', 'all');

    // enqueue javascript files
    wp_enqueue_script('modernizr', get_template_directory_uri().'/js/vendor/modernizr-2.8.3.min.js', array(), '2.8.3', false);
    wp_enqueue_script('jquery-1.12.0', get_template_directory_uri().'/js/vendor/jquery-1.12.0.min.js', array(), '1.12', true);
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
    wp_enqueue_script('owl.carousel', get_template_directory_uri().'/js/owl.carousel.min.js', array(), false, true);
    wp_enqueue_script('plugins', get_template_directory_uri().'/js/plugins.js', array(), false, true);
    wp_enqueue_script('slick', get_template_directory_uri().'/js/slick.min.js', array(), false, true);
    wp_enqueue_script('waypoints', get_template_directory_uri().'/js/waypoints.min.js', array(), false, true);
    wp_enqueue_script('main', get_template_directory_uri().'/js/main.js', array(), false, true);
}add_action('wp_enqueue_scripts', 'tasfiu_css_js');